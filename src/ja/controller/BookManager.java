package ja.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

import ja.model.entities.Book;
import ja.model.entities.Classroom;
import ja.model.entities.Course;
import ja.model.entities.Group;
import ja.model.entities.Study;
import ja.model.entities.TPSession;
import ja.model.entities.Teacher;

public class BookManager extends AbstractController {
    public static Set<Book> books = factory.getBookDao().getAll();

    /**
     * Faire une réservation
     */
    public void addBook( Course course, LocalDateTime debut, LocalDateTime fin, Classroom classroom ) {
        Book book = new Book( course, classroom );
        book.setStartTime( debut );
        book.setEndTime( fin );
        factory.getBookDao().create( book );
        books.add( book );

    }

    /**
     * Obtenir la liste des reservations concernant une salle
     * 
     * @param classroom
     * @return
     */
    public Set<Book> getBook( Classroom classroom ) {
        LocalDateTime today = LocalDateTime.now();
        return books.stream().filter( b -> b.getStartTime().isAfter( today ) || b.getStartTime().equals( today ) )
                .collect( Collectors.toSet() );
    }

    /**
     * Obtenir la liste des reservations faites à une date donnée
     * 
     * @param date
     * @return
     */
    public Set<Book> getBook( LocalDate date ) {
        return books.stream().filter( b -> b.getDateBook().equals( date ) ).collect( Collectors.toSet() );
    }

    /**
     * Annuler une reservation
     * 
     * @param book
     * @return
     */
    public boolean cancelBook( Book book ) {
        if ( books.contains( book ) && book.getStartTime().isAfter( LocalDateTime.now() ) ) {
            factory.getBookDao().delete( book );
            System.out.println( "ok" );
            factory.getCourseDao().delete( book.getCourse() );
            books.remove( book );
            return true;
        } else {
        }
        return false;
    }

    /**
     * Renvoie les reservations de cours pour une promotion donnée à partir
     * d'une date donnée
     * 
     * @param study
     *            :promotion dont on recherches l'emploi du temps
     * @param date:
     *            date à partir de laquelle on recherche les cours programmés
     * @return : l'ensemble des reservations respectant les critères données
     */
    public static Set<Book> getBook( Study study, LocalDateTime date ) {

        return books.stream()
                .filter( b -> b.getEndTime().isAfter( date ) )
                .collect( Collectors.toSet() );
    }

    /**
     * Obtenir les salles utilisées par une professeur donné
     * 
     * @param teacher
     * @return
     */
    public static Set<Classroom> getClassroom( Teacher teacher ) {
        Set<Course> coursDonnes = CourseManager.courses.stream()
                .filter( c -> c.getTeacher().getIdPerson() == teacher.getIdPerson() )
                .collect( Collectors.toSet() );
        return BookManager.books.stream().filter( b -> coursDonnes.contains( b.getCourse() ) )
                .map( Book::getClassroom ).collect( Collectors.toSet() );

    }

    /**
     * Obtenir la liste des salles utilisées par un groupe d'étudiants
     * 
     * @param group
     * @return
     */
    public static Set<Classroom> getClassroom( Group group ) {
        Set<Course> coursSuivis = CourseManager.courses.stream()
                .filter( c -> ( c instanceof TPSession ) || ( (TPSession) c ).getGroup().equals( group ) )
                .collect( Collectors.toSet() );
        return BookManager.books.stream().filter( b -> coursSuivis.contains( b ) )
                .map( Book::getClassroom ).collect( Collectors.toSet() );
    }
}
