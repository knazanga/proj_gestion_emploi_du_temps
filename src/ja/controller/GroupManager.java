package ja.controller;

import java.util.Set;
import java.util.stream.Collectors;

import ja.model.entities.Group;
import ja.model.entities.Student;
import ja.model.entities.Study;

public class GroupManager extends AbstractController {
    public static Set<Group>   groups   = factory.getGroupDao().getAll();
    public static Set<Student> students = factory.getStudentDao().getAll();

    /**
     * Recuperer les groupes pour une filiere donnée
     * 
     * @param study
     * @return
     */

    public static Set<Group> getStudyGroups( Study study ) {
        groups.stream().filter( g -> g.getStudy().equals( study ) ).collect( Collectors.toSet() );
        return groups;
    }

    /**
     * Recuperer les etudiants d'un groupe donné
     * 
     * @param group
     * @return
     */
    public static Set<Student> getStudents( Group group ) {
        return students.stream()
                .filter( s -> s.getGroup() != null && s.getGroup().getIdGroup() == group.getIdGroup()
                        && s.getStudy().getIdStudy() == group.getStudy().getIdStudy() )
                .collect( Collectors.toSet() );
    }

    /**
     * Recuperer les etudiants n'appartenant à aucun groupe
     * 
     * @param study
     * @return
     */
    public static Set<Student> noGroupStudents( Study study ) {
        return students.stream()
                .filter( s -> s.getStudy().getIdStudy() == study.getIdStudy() && s.getGroup() == null )
                .collect( Collectors.toSet() );
    }

    /**
     * Ajouter un etudiant à un groupe donné
     * 
     * @param student
     * @param group
     */
    public static void addStudentToGroup( Student student, Group group ) {
        student.setGroup( group );
        factory.getStudentDao().update( student );
    }

    /**
     * Retirer un etudiant d'un groupe donné
     * 
     * @param student
     */
    public static void removeStudentFromGroup( Student student ) {
        student.setGroup( null );
        factory.getStudentDao().update( student );

    }

    /**
     * Ajouter un groupe
     * 
     * @param idGroup
     * @param study
     * @return
     */
    public static boolean addGroup( int idGroup, Study study ) {
        Group group = new Group( idGroup, study );
        factory.getGroupDao().create( group );
        groups.add( group );
        return true;
    }

    /**
     * Supprimer un groupe
     * 
     * @param group
     * @return
     */
    public static boolean deleteGroup( Group group ) {
        Set<Student> students = getStudents( group );
        for ( Student s : students ) {
            removeStudentFromGroup( s );
        }
        factory.getGroupDao().delete( group );
        return true;
    }

}
