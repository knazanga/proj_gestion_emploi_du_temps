package ja.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ja.model.entities.Building;

public class BuildingManager extends AbstractController {

    public static Set<Building> buildings = factory.getBuildingDao().getAll();

    public boolean addBuilding( String idBuilding, String description ) {
        if ( buildings.stream().noneMatch( b -> b.getIdBuilding().equals( idBuilding ) ) ) {
            Building build = new Building( idBuilding, description );
            buildings.add( build );
            factory.getBuildingDao().create( build );
            return true;
        }
        return false;
    }

    public void deleteBuilding( String idBuilding ) {
        if ( buildings.stream().anyMatch( b -> b.getIdBuilding().equals( idBuilding ) ) ) {
            Building build = new Building( idBuilding, "" );
            buildings.remove( build );
            factory.getBuildingDao().delete( build );
        }
    }

    public Building getBuilding( String idBuilding ) {
        for ( Building b : buildings ) {
            if ( b.getIdBuilding().equals( idBuilding ) )
                return b;
        }
        return null;
    }

    public Map<Integer, Building> getAsMap() {
        Map<Integer, Building> builds = new HashMap<Integer, Building>();
        int i = 0;
        for ( Building b : buildings ) {
            i++;
            builds.put( i, b );
        }
        return builds;
    }

}
