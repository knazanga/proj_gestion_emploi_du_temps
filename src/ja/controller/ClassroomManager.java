package ja.controller;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

import ja.model.entities.Book;
import ja.model.entities.Building;
import ja.model.entities.ChemistryLab;
import ja.model.entities.Classroom;
import ja.model.entities.ComputerRoom;
import ja.model.entities.LectureRoom;

public class ClassroomManager extends AbstractController {
    public static Set<Classroom> classrooms;

    public ClassroomManager() {
        classrooms = factory.getClassroomDao().getAll();
    }

    /**
     * Ajouter une salle dans un batiment
     * 
     * @param idClass
     * @param idBuilding
     * @param capacity
     * @param type
     * @return
     */
    public boolean addClassroom( int idClass, String idBuilding, int capacity, String type ) {
        BuildingManager bManager = new BuildingManager();
        Building build = bManager.getBuilding( idBuilding );
        if ( build != null ) {
            Classroom classroom = getClassroom( idClass, build );
            if ( classroom == null ) {
                if ( type.equals( Classroom.TYPE_CHEMISTRYLAB ) )
                    classroom = new ChemistryLab( idClass, build, capacity );
                else if ( type.equals( Classroom.TYPE_COMPUTERROOM ) )
                    classroom = new ComputerRoom( idClass, build, capacity );
                else
                    classroom = new LectureRoom( idClass, build, capacity );
                factory.getClassroomDao().create( classroom );
                classrooms.add( classroom );
                return true;
            }
        }
        return false;
    }

    /**
     * Supprimer une salle
     * 
     * @param idClass
     * @param idBuilding
     * @return
     */
    public boolean deleteClassroom( int idClass, String idBuilding ) {
        BuildingManager bManager = new BuildingManager();
        Building build = bManager.getBuilding( idBuilding );
        if ( build != null ) {
            Classroom classroom = getClassroom( idClass, build );
            if ( classroom != null ) {
                factory.getClassroomDao().delete( classroom );
                classrooms.remove( classroom );
                return true;
            }
        }
        return false;
    }

    /**
     * Recuperer les infos sur une salle connaissant son numero et le batiment
     * 
     * @param classroom
     * @param building
     * @return
     */
    public Classroom getClassroom( int classroom, Building building ) {
        for ( Classroom c : classrooms ) {
            if ( c.getIdClassroom() == classroom && c.getBuilding().equals( building ) )
                return c;
        }
        return null;
    }

    /**
     * Recuperer les salles libres pour un creneau donné
     * 
     * @param debut
     * @param fin
     * @return
     */
    public Set<Classroom> getFreeRooms( LocalDateTime debut, LocalDateTime fin ) {
        Set<Book> books = BookManager.books;
        Set<Classroom> unFreeRooms = books.stream()
                .filter( b -> isOverlappingSlots( debut, fin, b.getStartTime(), b.getEndTime() ) )
                .map( Book::getClassroom ).collect( Collectors.toSet() );
        return classrooms.stream().filter( c -> unFreeRooms.stream().noneMatch( b -> b.equals( c ) ) )
                .collect( Collectors.toSet() );
    }

    /**
     * Methode utilitaire pour verifier si deux créneaux se chevauchent
     * 
     * @param debut1
     * @param fin1
     * @param debut
     * @param fin
     * @return
     */
    public static boolean isOverlappingSlots( LocalDateTime debut1, LocalDateTime fin1, LocalDateTime debut,
            LocalDateTime fin ) {
        return ( debut1.isBefore( debut ) && fin1.isAfter( fin ) )
                | ( debut1.isAfter( debut ) && debut1.isBefore( fin ) )
                | ( fin1.isAfter( debut ) && fin1.isBefore( fin ) );
    }

    public Set<Classroom> getClassrooms() {
        return classrooms;
    }

}
