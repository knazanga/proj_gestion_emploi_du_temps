package ja.controller;

import java.util.Set;

import ja.model.entities.Course;
import ja.model.entities.Group;
import ja.model.entities.Module;
import ja.model.entities.Study;
import ja.model.entities.TPSession;
import ja.model.entities.Teacher;

public class CourseManager extends AbstractController {

    public static Set<Study>  studies = factory.getStudyDao().getAll();
    public static Set<Course> courses = factory.getCourseDao().getAll();

    /**
     * Enregistrer un cours suite à une réservation
     * 
     * @param study
     * @param module
     * @param teacher
     * @param type
     * @param group
     * @return
     */
    public Course addCourse( Study study, Module module, Teacher teacher, String type, Group group ) {

        if ( type.equals( Course.TYPE_COURSE_TP ) ) {

            TPSession cours = new TPSession( module, teacher, study );
            cours.setGroup( group );
            factory.getTPSessionDao().create( cours );
            return cours;
        } else {
            Course cours = new Course( module, teacher, study );
            factory.getCourseDao().create( cours );
            return cours;
        }

    }

}
