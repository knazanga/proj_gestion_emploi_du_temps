package ja.test;

import ja.controller.GroupManager;
import ja.model.entities.Study;

public class Test {

    public static void main( String[] agrs ) {
        Study study = new Study();
        study.setIdStudy( 8 );
        GroupManager.noGroupStudents( study ).forEach( System.out::println );
    }

}
