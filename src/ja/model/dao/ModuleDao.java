package ja.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import ja.model.entities.Module;
import ja.model.entities.Teacher;

public class ModuleDao extends TemplateDao<Module> {

    public ModuleDao( Connection connexion ) {
        super( connexion );
        // TODO Auto-generated constructor stub
    }

    @Override
    public void completeObject( Module obj, ResultSet keys ) throws SQLException {
        obj.setIdModule( keys.getInt( 1 ) );

    }

    @Override
    public Module map( ResultSet result ) throws SQLException {
        Module module = new Module();
        module.setIdModule( result.getInt( "idModule" ) );
        module.setName( result.getString( "name" ) );
        module.setTeachers( getTeachers( module ) );
        return module;
    }

    @Override
    public PreparedStatement gettingSql( Object[] ids ) throws SQLException {
        String query = "SELECT * FROM Module WHERE idModule=?";
        PreparedStatement pStatement = this.connect.prepareStatement( query );
        pStatement.setObject( 1, ids[0] );
        return pStatement;
    }

    @Override
    public PreparedStatement creatingSql( Module obj ) throws SQLException {
        String query = "INSERT INTO Module (name) VALUES(?)";
        PreparedStatement pStatement = this.connect.prepareStatement( query, Statement.RETURN_GENERATED_KEYS );
        pStatement.setString( 1, obj.getName() );
        return pStatement;
    }

    @Override
    public boolean needReturnedValue() {
        return true;
    }

    @Override
    public PreparedStatement gettingAllSql() throws SQLException {
        String query = "SELECT * FROM Module";
        PreparedStatement pStatement = this.connect.prepareStatement( query );
        return pStatement;
    }

    @Override
    public PreparedStatement deletingSql( Module obj ) throws SQLException {
        String query = "DELETE FROM Module WHERE idModule=?";
        PreparedStatement pStatement = this.connect.prepareStatement( query );
        pStatement.setInt( 1, obj.getIdModule() );
        return pStatement;
    }

    public Set<Teacher> getTeachers( Module obj ) {
        Set<Teacher> teachers = new HashSet<Teacher>();
        String query = "SELECT T.idTeacher, firstName, lastName FROM Teacher T, TeacherModule M WHERE M.idTeacher=T.idTeacher AND M.idModule=?";
        try {
            PreparedStatement pStatement = this.connect.prepareStatement( query );
            pStatement.setInt( 1, obj.getIdModule() );
            ResultSet result = pStatement.executeQuery();
            TeacherDao teacherDao = new TeacherDao( this.connect );
            while ( result.next() ) {
                teachers.add( teacherDao.map( result ) );
            }

        } catch ( SQLException e ) {
            e.printStackTrace();
        }
        return teachers;
    }

}
