package ja.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
    public static final String DRIVER   = "com.mysql.jdbc.Driver";

    public static final String URL      = "jdbc:mysql://localhost:3306/db_ja";
    public static final String USER     = "java";
    public static final String PASSWORD = "java";

    public static Connection connect() {

        Connection connexion = null;
        try {
            Class.forName( DRIVER );
            connexion = DriverManager.getConnection( URL, USER, PASSWORD );
        } catch ( ClassNotFoundException e ) {
            e.printStackTrace();
        } catch ( SQLException e ) {
            e.printStackTrace();
        }

        return connexion;
    }

}