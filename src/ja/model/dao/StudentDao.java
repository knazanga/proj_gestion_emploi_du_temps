package ja.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ja.model.entities.Group;
import ja.model.entities.Student;
import ja.model.entities.Study;

public class StudentDao extends TemplateDao<Student> {

    public StudentDao( Connection connexion ) {
        super( connexion );

    }

    @Override
    public void completeObject( Student obj, ResultSet keys ) throws SQLException {
        obj.setIdPerson( keys.getInt( 1 ) );

    }

    @Override
    public PreparedStatement creatingSql( Student obj ) throws SQLException {
        String query = "INSERT INTO Student (firstName, lastName) VALUES(?, ?, ?, ?)";
        PreparedStatement pStatement = this.connect.prepareStatement( query, Statement.RETURN_GENERATED_KEYS );
        pStatement.setString( 1, obj.getFirstName() );
        pStatement.setString( 2, obj.getLastName() );
        pStatement.setInt( 3, obj.getStudy().getIdStudy() );
        pStatement.setInt( 4, obj.getGroup().getIdGroup() );
        return pStatement;
    }

    @Override
    public boolean needReturnedValue() {
        return true;
    }

    @Override
    public Student map( ResultSet result ) throws SQLException {
        Student student = new Student();
        student.setIdPerson( result.getInt( "idStudent" ) );
        student.setFirstName( result.getString( "firstName" ) );
        student.setLastName( result.getString( "lastName" ) );
        int idStudy = result.getInt( "idStudy" );
        int idGroup = result.getInt( "idGroup" );
        if ( idStudy != 0 ) {
            Dao<Study> daoStudy = new StudyDao( this.connect );
            student.setStudy( daoStudy.get( idStudy ) );
        }

        if ( idGroup != 0 ) {
            Dao<Group> daoGroup = new GroupDao( this.connect );
            student.setGroup( daoGroup.get( idGroup, idStudy ) );
        }

        return student;

    }

    @Override
    public PreparedStatement gettingSql( Object[] ids ) throws SQLException {
        String query = "SELECT * FROM Student WHERE idStudent=?";
        PreparedStatement pStatement = this.connect.prepareStatement( query );
        pStatement.setObject( 1, ids[0] );
        return pStatement;
    }

    @Override
    public PreparedStatement gettingAllSql() throws SQLException {
        String query = "SELECT * FROM Student";
        PreparedStatement pStatement = this.connect.prepareStatement( query );
        return pStatement;
    }

    @Override
    public PreparedStatement deletingSql( Student obj ) throws SQLException {
        String query = "DELETE FROM Student WHERE idStudent=?";
        PreparedStatement pStatement = this.connect.prepareStatement( query );
        pStatement.setInt( 1, obj.getIdPerson() );
        return pStatement;
    }

    @Override
    public void update( Student obj ) {
        String query = "UPDATE Student SET idGroup=? WHERE idStudent=?";
        try {
            PreparedStatement pStatement = this.connect.prepareStatement( query );
            if ( obj.getGroup() != null )
                pStatement.setInt( 1, obj.getGroup().getIdGroup() );
            else
                pStatement.setObject( 1, null );
            pStatement.setInt( 2, obj.getIdPerson() );
            pStatement.executeUpdate();
        } catch ( SQLException e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
