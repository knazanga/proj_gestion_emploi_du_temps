package ja.model.dao;

import java.sql.Connection;

import ja.model.entities.Book;
import ja.model.entities.Building;
import ja.model.entities.Classroom;
import ja.model.entities.Course;
import ja.model.entities.Group;
import ja.model.entities.Level;
import ja.model.entities.Module;
import ja.model.entities.Student;
import ja.model.entities.Study;
import ja.model.entities.TPSession;
import ja.model.entities.Teacher;

public class DaoFactory {
    private Connection connection = Connect.connect();

    public Dao<Building> getBuildingDao() {
        return new BuildingDao( this.connection );
    }

    public Dao<Level> getLevelDao() {
        return new LevelDao( this.connection );
    }

    public Dao<Course> getCourseDao() {
        return new CourseDao( this.connection );
    }

    public Dao<Classroom> getClassroomDao() {
        return new ClassroomDao( this.connection );
    }

    public Dao<Group> getGroupDao() {
        return new GroupDao( this.connection );
    }

    public Dao<Module> getModuleDao() {
        return new ModuleDao( this.connection );
    }

    public Dao<Study> getStudyDao() {
        return new StudyDao( this.connection );
    }

    public Dao<Student> getStudentDao() {
        return new StudentDao( this.connection );
    }

    public Dao<Teacher> getTeacherDao() {
        return new TeacherDao( this.connection );
    }

    public Dao<TPSession> getTPSessionDao() {
        return new TPSessionDao( this.connection );
    }

    public Dao<Book> getBookDao() {
        return new BookDao( this.connection );
    }

}
