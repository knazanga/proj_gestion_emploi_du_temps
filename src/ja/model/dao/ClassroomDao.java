package ja.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ja.model.entities.Building;
import ja.model.entities.ChemistryLab;
import ja.model.entities.Classroom;
import ja.model.entities.ComputerRoom;
import ja.model.entities.LectureRoom;

public class ClassroomDao extends TemplateDao<Classroom> {

    public ClassroomDao( Connection connexion ) {
        super( connexion );
    }

    @Override
    public void completeObject( Classroom obj, ResultSet keys ) {

    }

    @Override
    public PreparedStatement creatingSql( Classroom obj ) throws SQLException {
        String query = "INSERT INTO Classroom VALUES(?, ?, ?, ?)";

        Dao<Building> dao = new BuildingDao( this.connect );
        if ( dao.get( obj.getBuilding().getIdBuilding() ) == null )
            dao.create( obj.getBuilding() );

        PreparedStatement pStatement = this.connect.prepareStatement( query );
        pStatement.setInt( 1, obj.getIdClassroom() );
        pStatement.setInt( 3, obj.getCapacity() );
        pStatement.setString( 2, obj.getType() );
        pStatement.setString( 4, obj.getBuilding().getIdBuilding() );
        return pStatement;
    }

    @Override
    public boolean needReturnedValue() {
        return false;
    }

    @Override
    public Classroom map( ResultSet result ) throws SQLException {
        Classroom classroom;

        String classType = result.getString( "type" );

        if ( classType.equals( Classroom.TYPE_LECTUREROOM ) )
            classroom = new LectureRoom();
        else if ( classType.equals( Classroom.TYPE_COMPUTERROOM ) )
            classroom = new ComputerRoom();
        else
            classroom = new ChemistryLab();

        classroom.setIdClassroom( result.getInt( "idClassroom" ) );
        classroom.setCapacity( result.getInt( "capacity" ) );

        String idBuilding = result.getString( "idBuilding" );
        Dao<Building> daoBuilding = new BuildingDao( this.connect );
        classroom.setBuilding( daoBuilding.get( idBuilding ) );

        return classroom;
    }

    @Override
    public PreparedStatement gettingSql( Object[] ids ) throws SQLException {
        String query = "SELECT * FROM Classroom WHERE idClassroom=? AND idBuilding=?";
        PreparedStatement pStatement = this.connect.prepareStatement( query );
        pStatement.setObject( 1, ids[0] );
        pStatement.setObject( 2, ids[1] );
        return pStatement;
    }

    @Override
    public PreparedStatement gettingAllSql() throws SQLException {
        String query = "SELECT * FROM Classroom";
        return this.connect.prepareStatement( query );
    }

    @Override
    public PreparedStatement deletingSql( Classroom obj ) throws SQLException {
        String query = "DELETE FROM Classroom WHERE idClassroom=? AND idBuilding=?";
        PreparedStatement pStatement = this.connect.prepareStatement( query );
        pStatement.setInt( 1, obj.getIdClassroom() );
        pStatement.setString( 2, obj.getBuilding().getIdBuilding() );
        return pStatement;
    }

}
