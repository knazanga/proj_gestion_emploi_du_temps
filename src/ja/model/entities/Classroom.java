package ja.model.entities;

public abstract class Classroom {

    public static final String TYPE_LECTUREROOM  = "Salle pour cours magistral";
    public static final String TYPE_CHEMISTRYLAB = "Salle de chimie";
    public static final String TYPE_COMPUTERROOM = "Salle machine";

    protected int              idClassroom;
    protected Building         building;
    protected int              capacity;
    protected String           type;

    public Classroom() {

    }

    public Classroom( int idClassroom, Building building ) {
        super();
        this.idClassroom = idClassroom;
        this.building = building;
    }

    public Classroom( int idClassroom, Building building, int capacity ) {

        this.idClassroom = idClassroom;
        this.building = building;
        this.capacity = capacity;
    }

    public int getIdClassroom() {
        return idClassroom;
    }

    public void setIdClassroom( int idClassroom ) {
        this.idClassroom = idClassroom;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity( int capacity ) {
        this.capacity = capacity;
    }

    public String getType() {
        return type;
    }

    public void setType( String type ) {
        this.type = type;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding( Building building ) {
        this.building = building;
    }

    @Override
    public boolean equals( Object obj ) {
        return this.getIdClassroom() == ( (Classroom) obj ).getIdClassroom()
                && this.getBuilding().equals( ( (Classroom) obj ).getBuilding() );
    }

    public String toString() {
        return this.building.getIdBuilding() + this.idClassroom + " " + this.getType() + " (" + this.getCapacity()
                + " places)";
    }

}
