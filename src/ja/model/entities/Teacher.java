package ja.model.entities;

public class Teacher extends Person {

    public Teacher( String firstName, String lastName ) {
        super( firstName, lastName );
    }

    public Teacher() {
        super();
    }

    public String toString() {
        return this.firstName + " " + this.lastName;
    }
}
