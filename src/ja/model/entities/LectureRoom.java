package ja.model.entities;

public class LectureRoom extends Classroom {

    public LectureRoom() {
        this.type = TYPE_LECTUREROOM;
    }

    public LectureRoom( int id, Building building ) {
        super( id, building );
        this.type = TYPE_LECTUREROOM;
    }

    public LectureRoom( int id, Building building, int capacity ) {
        super( id, building, capacity );
        this.type = TYPE_LECTUREROOM;
    }

}
