package ja.model.entities;

import java.util.HashSet;
import java.util.Set;

public class Module {

    private int          idModule;
    private String       name;
    private Set<Teacher> teachers;

    public Module() {
        this.teachers = new HashSet<Teacher>();

    }

    public Module( int idModule, String name ) {
        this.idModule = idModule;
        this.name = name;
    }

    public Module( String name ) {
        this.name = name;
    }

    public int getIdModule() {
        return idModule;
    }

    public void setIdModule( int idModule ) {
        this.idModule = idModule;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public Set<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers( Set<Teacher> teachers ) {
        this.teachers = teachers;
    }

    public String toString() {
        return this.name;
    }

}
