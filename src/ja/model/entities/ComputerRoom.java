package ja.model.entities;

public class ComputerRoom extends Classroom {

    public ComputerRoom() {
        this.type = TYPE_COMPUTERROOM;
    }

    public ComputerRoom( int id, Building building ) {
        super( id, building );
        this.type = TYPE_COMPUTERROOM;
    }

    public ComputerRoom( int id, Building building, int capacity ) {
        super( id, building, capacity );
        this.type = TYPE_COMPUTERROOM;
    }

}
