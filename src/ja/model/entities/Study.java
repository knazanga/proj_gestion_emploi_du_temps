package ja.model.entities;

import java.util.HashSet;
import java.util.Set;

public class Study {

    private int          idStudy;
    private String       name;
    private Set<Student> students;
    private Set<Module>  modules;
    private Set<Group>   groups;

    public Study() {
        this.students = new HashSet<Student>();
        this.modules = new HashSet<Module>();
        this.groups = new HashSet<Group>();
    }

    public Study( String name ) {
        this.name = name;
        this.students = new HashSet<Student>();
    }

    public int getIdStudy() {
        return idStudy;
    }

    public void setIdStudy( int idStudy ) {
        this.idStudy = idStudy;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents( Set<Student> students ) {
        this.students = students;
    }

    public void addStudent( Student student ) {
        this.students.add( student );
    }

    public void removeStudent( Student student ) {
        this.students.remove( student );
    }

    public Set<Module> getModules() {
        return modules;
    }

    public void setModules( Set<Module> modules ) {
        this.modules = modules;
    }

    public String toString() {
        return this.name;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups( Set<Group> groups ) {
        this.groups = groups;
    }

    @Override
    public boolean equals( Object obj ) {
        return this.idStudy == ( (Study) obj ).getIdStudy();
    }

}
