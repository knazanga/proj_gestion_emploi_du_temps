package ja.model.entities;

public class ChemistryLab extends Classroom {

    public ChemistryLab() {
        this.type = TYPE_CHEMISTRYLAB;
    }

    public ChemistryLab( int id, Building building ) {
        super( id, building );
        this.type = TYPE_CHEMISTRYLAB;
    }

    public ChemistryLab( int id, Building building, int capacity ) {
        super( id, building, capacity );
        this.type = TYPE_CHEMISTRYLAB;
    }

}
