package ja.view;

import static ja.view.TypeUtilities.getInt;
import static ja.view.TypeUtilities.printMap;
import static ja.view.TypeUtilities.transformToMap;

import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ja.controller.CourseManager;
import ja.controller.GroupManager;
import ja.model.entities.Group;
import ja.model.entities.Student;
import ja.model.entities.Study;

public class ViewGroup {

    public static void run( String[] args ) {

        int choice;

        System.out.println( "				 .. .. .. .. .. .. .. .. .. .. .. .. .. .. ... " );
        System.out.println( "				:					      :" );
        System.out.println( "				: 		Gestion des groupes	      :" );
        System.out.println( "				:					      :" );
        System.out.println( "				 .. .. .. .. .. .. .. .. .. .. .. .. .. .. ... " );
        System.out.println();
        System.out.println();

        Stream<String> stream = Stream.of( "MENU" );
        stream.forEach(
                s -> System.out.println( "................................" + s + ".............................." ) );
        System.out.println();
        System.out.println( "	*	1.  Consulter les groupes de TP\t\t\t*" );
        System.out.println( "	*	2.  Ajouter/Supprimer un groupe\t\t\t*" );
        System.out.println( "	*	3.  Ajouter/Supprimer des étudiants d'un groupe\t*" );
        System.out.println( "	*	4.  Retour à l'accueil\t\t\t\t*" );
        System.out.println( "	*	5.  Quitter\t\t\t\t\t*" );
        System.out.println( ".................................................................." );

        do {
            System.out.println( "Entrez votre choix [1..5] :" );
            Scanner scanner = new Scanner( System.in );
            choice = scanner.nextInt();
            switch ( choice ) {
            case 1:
                viewGroups( scanner );
                ViewGroup.run( args );
                break;
            case 2:
                addingGroup( scanner );
                ViewGroup.run( args );
                break;
            case 3:
                System.out.println( "Fonctionalité en conception!" );
                break;
            case 4:
                System.out.println( "Retour à l'accueil" );
                MainView.main( args );
                System.out.println();
                break;
            case 5:
                System.out.println( "Bye Bye!..." );
                break;
            default:
                System.out.println( "Choix non valide, réessayez !" );
                break;
            }
            scanner.close();
        } while ( choice == 0 || choice > 6 );

    }

    public static void viewGroups( Scanner sc ) {
        Map<Integer, Study> studies = transformToMap( CourseManager.studies );
        System.out.println( "Veuillez selection une promotion:" );
        int idStudy = 0;
        do {
            printMap( studies );
            idStudy = getInt( sc );
        } while ( !studies.containsKey( idStudy ) );

        Map<Integer, Group> groups = transformToMap( GroupManager.getStudyGroups( studies.get( idStudy ) ) );
        Map<Integer, Student> groupStudents = null;
        int idGroup = 0;
        if ( !groups.isEmpty() ) {
            System.out.println( "Selectionner un groupe pour voir la liste des étudiants de ce groupe:" );
            do {
                printMap( groups );
                idGroup = getInt( sc );
            } while ( !groups.containsKey( idGroup ) );

            System.out.println( "Les étudiants de ce groupe sont:" );
            groupStudents = transformToMap( GroupManager.getStudents( groups.get( idGroup ) ) );
            printMap( groupStudents );
        }
        System.out.println( "Quelle opération voulez-vous effectuer sur ce groupe?:" );
        System.out.println( "\t0 Rien du tout" );
        System.out.println( "\t1 Ajouter des étudiants" );
        System.out.println( "\t2 Supprimer des étudiants" );
        int ch = 0;
        do {
            ch = getInt( sc );
        } while ( ch < 0 || ch > 2 );
        Study study = studies.get( idStudy );
        Map<Integer, Student> noGroupStudents = transformToMap(
                GroupManager.noGroupStudents( study ) );
        if ( ch == 1 || ch == 2 ) {
            if ( ch == 1 )

                printMap( noGroupStudents );
            else
                printMap( groupStudents );
            int idStart = 0;
            int idEnd = 0;
            do {
                String str = sc.nextLine();
                try {
                    if ( str.matches( "^\\d+-\\d+$" ) ) {
                        idStart = Integer.parseInt( str.split( "-" )[0] );
                        idEnd = Integer.parseInt( str.split( "-" )[1] );
                    } else {
                        idStart = Integer.parseInt( str );
                    }
                } catch ( Exception e ) {
                    idStart = -1;
                }
            } while ( idStart <= 0 );

            if ( idEnd == 0 ) {
                if ( ch == 1 ) {
                    GroupManager.addStudentToGroup( noGroupStudents.get( idStart ), groups.get( idGroup ) );
                } else {
                    GroupManager.removeStudentFromGroup( groupStudents.get( idStart ) );
                }
            } else {
                int t = idStart;
                if ( idEnd < idStart ) {
                    idStart = idEnd;
                    idEnd = t;
                }

                for ( int i = idStart; i <= idEnd; i++ ) {
                    if ( ch == 1 ) {
                        GroupManager.addStudentToGroup( noGroupStudents.get( i ), groups.get( idGroup ) );
                    } else {
                        GroupManager.removeStudentFromGroup( groupStudents.get( i ) );
                    }
                }

            }
            System.out.println( "Opération(s) réalisée(s) avec succès!" );
        }
    }

    public static void addingGroup( Scanner sc ) {
        Map<Integer, Study> studies = transformToMap( CourseManager.studies );
        System.out.println( "Veuillez selection une promotion:" );
        int idStudy = 0;
        do {
            printMap( studies );
            idStudy = getInt( sc );
        } while ( !studies.containsKey( idStudy ) );
        Map<Integer, Group> groups = transformToMap( GroupManager.getStudyGroups( studies.get( idStudy ) ) );
        printMap( groups );
        System.out.println( "Que voulez vous faire?:" );
        System.out.println( "\t0 Rien du tout" );
        System.out.println( "\t1 Ajouter un groupe" );
        System.out.println( "\t2 Supprimer un groupe" );
        int ch = 0;
        do {
            ch = getInt( sc );
        } while ( ch < 0 || ch > 2 );
        int idGroup = 0;
        if ( ch == 1 ) {
            java.util.Set<Integer> groupIds = groups.values().stream().map( Group::getIdGroup )
                    .collect( Collectors.toSet() );
            System.out.println( "Veuillez entrer le numer du groupe:" );
            do {
                idGroup = getInt( sc );
            } while ( groupIds.contains( new Integer( idGroup ) ) );

            if ( GroupManager.addGroup( idGroup, studies.get( idStudy ) ) )
                System.out.println( "Votre groupe a été ajouté avec succès!" );
            else
                System.out.println( "Echec de l'ajout du groupe." );
        } else if ( ch == 2 ) {
            System.out.println( "Selectionner le groupe à supprimer:" );
            do {
                printMap( groups );
                idGroup = getInt( sc );
            } while ( !groups.containsKey( idGroup ) );

            if ( GroupManager.deleteGroup( groups.get( idGroup ) ) )
                System.out.println( "Groupe supprimé avec succès!" );
            else
                System.out.println( "Echec de la suppression du groupe" );
        }

    }

}
