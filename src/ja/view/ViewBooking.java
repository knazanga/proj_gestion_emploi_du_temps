package ja.view;

import static ja.view.TypeUtilities.getInt;
import static ja.view.TypeUtilities.printMap;
import static ja.view.TypeUtilities.saisieDate;
import static ja.view.TypeUtilities.saisieHeure;
import static ja.view.TypeUtilities.transformToMap;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Stream;

import ja.controller.BookManager;
import ja.controller.ClassroomManager;
import ja.controller.CourseManager;
import ja.controller.GroupManager;
import ja.model.entities.Book;
import ja.model.entities.Classroom;
import ja.model.entities.Course;
import ja.model.entities.Group;
import ja.model.entities.Module;
import ja.model.entities.Study;
import ja.model.entities.Teacher;

public class ViewBooking {

    public static void run( String[] args ) {

        System.out.println( "				 .. .. .. .. .. .. .. .. .. .. .. .. .. .. ... " );
        System.out.println( "				:					      :" );
        System.out.println( "				: 		Réservation de salles 	      :" );
        System.out.println( "				:					      :" );
        System.out.println( "				 .. .. .. .. .. .. .. .. .. .. .. .. .. .. ... " );
        System.out.println();
        System.out.println();

        Stream<String> stream = Stream.of( "MENU" );
        stream.forEach(
                s -> System.out.println( "................................" + s + ".............................." ) );
        System.out.println();
        System.out.println( "   *   1.  Faire une réservation\t\t\t\t*" );
        System.out.println( "   *   2.  Annuler une réservation\t\t\t\t*" );
        System.out.println( "   *   3.  Afficher les salles libre pour un créneau donné\t*" );
        System.out.println( "   *   4.  Retour à l'accueil\t\t\t\t\t*" );
        System.out.println( "   *   5.  Quitter\t\t\t\t\t\t*" );
        System.out.println( ".................................................................." );
        int choice = 0;
        do {
            System.out.println( "Entrez votre choix [1..5] :" );
            Scanner scanner = new Scanner( System.in );
            try {
                choice = scanner.nextInt();
            } catch ( Exception e ) {
                choice = 0;
            }
            switch ( choice ) {
            case 1:
                bookingType( scanner );
                ViewBooking.run( args );
                break;
            case 2:
                cancelBook( scanner );
                ViewBooking.run( args );
                break;
            case 3:
                freeSlotTime( scanner );
                ViewBooking.run( args );
                break;
            case 4:
                System.out.flush();
                System.out.println( "Retour à l'accueil..." );
                MainView.main( args );
                System.out.println();
                break;
            case 5:
                System.out.println( "Bye Bye..." );
                break;
            default:
                System.out.println( "Choix non valide, réessayez !" );
                break;
            }
            scanner.close();
        } while ( choice == 0 || choice > 6 );

    }

    public static void bookingType( Scanner sc ) {
        System.out.println( "Veuillez saisir la date du cours (jj-mm-aaaa)" );
        LocalDate date = saisieDate( sc );
        System.out.println( "Veuillez saisir l'heure de  début du cours (hh-mm)" );
        LocalTime d = saisieHeure( sc );
        System.out.println( "Veuillez saisir l'heure de fin du cours (hh-mm)" );
        LocalTime f = saisieHeure( sc );
        LocalDateTime debut = LocalDateTime.of( date, d );
        LocalDateTime fin = LocalDateTime.of( date, f );
        ClassroomManager cManager = new ClassroomManager();
        Map<Integer, Classroom> freeRooms = transformToMap( cManager.getFreeRooms( debut, fin ) );
        int idRoom;
        do {
            System.out.println( "Veuillez sélectionner la salle à réserver" );
            printMap( freeRooms );
            idRoom = getInt( sc );
        } while ( !freeRooms.containsKey( idRoom ) );
        Map<Integer, Study> studies = transformToMap( CourseManager.studies );
        int idStudy = 0;
        do {
            System.out.println( "Veuillez sélectionner la promotion concerné." );
            printMap( studies );
            idStudy = getInt( sc );
        } while ( !studies.containsKey( idStudy ) );

        Map<Integer, Module> modules = transformToMap( studies.get( idStudy ).getModules() );
        int idModule = 0;
        do {
            System.out.println( "Veuillez selectionner le module" );
            printMap( modules );
            idModule = getInt( sc );

        } while ( !modules.containsKey( idModule ) );
        Map<Integer, Teacher> teachers = transformToMap( modules.get( idModule ).getTeachers() );
        int idTeacher = 0;
        do {
            printMap( teachers );
            System.out.println( "Selectionner le professeur qui va assurer le cours." );
            idTeacher = getInt( sc );
        } while ( !teachers.containsKey( idTeacher ) );

        int typeCourse = 0;
        System.out.println( "Selectionner le type du cours:" );
        do {
            System.out.println( "\t1 CM/TD" );
            System.out.println( "\t2 TP" );
            typeCourse = getInt( sc );
        } while ( typeCourse != 1 && typeCourse != 2 );

        Map<Integer, Group> groups = null;
        int idGroup = 0;
        if ( typeCourse == 2 ) {
            groups = transformToMap( GroupManager.getStudyGroups( studies.get( idStudy ) ) );
            if ( !groups.isEmpty() ) {
                System.out.println( "Selectionner le groupe concerné:" );
                do {
                    System.out.println( "\t0 Toute la classe" );
                    printMap( groups );
                    idGroup = getInt( sc );
                } while ( !groups.containsKey( idGroup ) && idGroup != 0 );
            }
        }
        CourseManager coursManager = new CourseManager();
        Course cours = null;
        if ( typeCourse == 1 ) {
            cours = coursManager.addCourse( studies.get( idStudy ), modules.get( idModule ),
                    teachers.get( idTeacher ), "CM", null );
        } else {
            cours = coursManager.addCourse( studies.get( idStudy ), modules.get( idModule ), teachers.get( idTeacher ),
                    "TP", groups.get( idGroup ) );

        }
        BookManager bookManager = new BookManager();
        bookManager.addBook( cours, debut, fin, freeRooms.get( idRoom ) );
        System.out.println( "Votre reservation a été effectuée avec succès!" );
    }

    public static void cancelBook( Scanner sc ) {
        System.out.println( "Veuillez saisir la date à laquelle vous avez fait votre réservation:" );
        LocalDate date = saisieDate( sc );
        BookManager bookManager = new BookManager();
        Map<Integer, Book> books = transformToMap( bookManager.getBook( date ) );
        System.out.println( "Veuillez entrer la date de votre reservation:" );
        int idBook = 0;
        do {
            printMap( books );
            idBook = getInt( sc );
        } while ( !books.containsKey( idBook ) );
        System.out.println( "Voulez vous vraiment annuler cette réservation(Oui)?" );
        System.out.println( "Tapez Oui pour valider et tout autre chose pour annuler:" );
        sc.nextLine();
        String ch = sc.nextLine();
        if ( ch.equalsIgnoreCase( "oui" ) ) {
            if ( bookManager.cancelBook( books.get( idBook ) ) ) {
                System.out.println( "Votre réservation a été annulée avec succès!" );
            } else {
                System.out.println( "Echec de l'annulation, delais dépassé" );
            }
        }
    }

    public static void freeSlotTime( Scanner sc ) {
        System.out.println( "Veuillez saisir la date du cours (jj-mm-aaaa)" );
        LocalDate date = saisieDate( sc );
        System.out.println( "Veuillez saisir l'heure de  début du cours (hh-mm)" );
        LocalTime d = saisieHeure( sc );
        System.out.println( "Veuillez saisir l'heure de fin du cours (hh-mm)" );
        LocalTime f = saisieHeure( sc );
        LocalDateTime debut = LocalDateTime.of( date, d );
        LocalDateTime fin = LocalDateTime.of( date, f );
        ClassroomManager cManager = new ClassroomManager();
        Map<Integer, Classroom> freeRooms = transformToMap( cManager.getFreeRooms( debut, fin ) );
        System.out.println( "Les salles libre pour le créneau demandé sont:" );
        printMap( freeRooms );
    }

}
