package ja.view;

import java.util.Scanner;
import java.util.stream.Stream;

public class MainView {

    public static void main( String[] args ) {

        int choice;

        System.out.println( "				 .. .. .. .. .. .. .. .. .. .. .. .. .. .. ... " );
        System.out.println( "				:					      :" );
        System.out.println( "				: Bienvenue sur le logiciel d'emploi du temps :" );
        System.out.println( "				:					      :" );
        System.out.println( "				 .. .. .. .. .. .. .. .. .. .. .. .. .. .. ... " );
        System.out.println();
        System.out.println();

        Stream<String> stream = Stream.of( "MENU" );
        stream.forEach(
                s -> System.out.println( "................................" + s + ".............................." ) );
        System.out.println();
        System.out.println( "*\t1. Consulter l'emploi du temps \t\t*" );
        System.out.println( "*\t2. Gestion des salles \t\t\t*" );
        System.out.println( "*\t3. Gestion des réservations \t\t*" );
        System.out.println( "*\t4. Gestion des groupes de TP \t\t*" );
        System.out.println( "*\t5. Quitter \t\t\t\t*" );
        System.out.println( ".................................................................." );

        do {
            System.out.println( "Entrez votre choix [1..5] :" );
            Scanner scanner = new Scanner( System.in );
            try {
                choice = scanner.nextInt();
            } catch ( Exception e ) {
                choice = 0;
            }
            switch ( choice ) {
            case 1:
                ViewTimeTable.run( args );
                break;
            case 2:
                ViewClassroom.run( args );
                break;
            case 3:
                ViewBooking.run( args );
                break;
            case 4:
                ViewGroup.run( args );
                break;
            case 5:
                System.out.println( "Bye Bye!..." );
                break;
            default:
                System.out.println( "Choix non valide, réessayez !" );
                continue;
            }
            scanner.close();
        } while ( choice == 0 || choice > 5 );

    }

}
