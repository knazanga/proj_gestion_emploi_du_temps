package ja.view;

import static ja.view.TypeUtilities.getInt;
import static ja.view.TypeUtilities.printMap;
import static ja.view.TypeUtilities.saisieDate;
import static ja.view.TypeUtilities.transformToMap;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Stream;

import ja.controller.BookManager;
import ja.controller.CourseManager;
import ja.model.entities.Book;
import ja.model.entities.Study;

public class ViewTimeTable {

    public static void run( String[] args ) {
        int choice;

        System.out.println( "				 .. .. .. .. .. .. .. .. .. .. .. .. .. .. ... " );
        System.out.println( "				:					      :" );
        System.out.println( "				: 		Emploi du temps	      	      :" );
        System.out.println( "				:					      :" );
        System.out.println( "				 .. .. .. .. .. .. .. .. .. .. .. .. .. .. ... " );
        System.out.println();
        System.out.println();

        Stream<String> stream = Stream.of( "MENU" );
        stream.forEach(
                s -> System.out.println( "................................" + s + ".............................." ) );
        System.out.println();
        System.out.println( "*\t1.  Afficher l'emploi du temps d'une promotion:\t\t*" );
        System.out.println( "*\t2.  Quels étudiants occupent telle salle?\t\t*" );
        System.out.println( "*\t3.  Quel professeur occupe telle salle?\t\t\t*" );
        System.out.println( "*\t4.  Retour à l'accueil\t\t\t\t\t*" );
        System.out.println( "*\t5.  Quitter\t\t\t\t\t\t*" );
        System.out.println( ".................................................................." );

        do {
            System.out.println( "Entrez votre choix [1..5]  :" );
            Scanner scanner = new Scanner( System.in );
            choice = getInt( scanner );
            switch ( choice ) {
            case 1:
                timeTables( scanner );
                ViewTimeTable.run( args );
                break;
            case 2:
                System.out.println( "Fonctionnalité en conception!" );
                ViewTimeTable.run( args );
                break;
            case 3:
                System.out.println( "Fonctionnalité en conception!" );
                ViewTimeTable.run( args );
                break;
            case 4:
                MainView.main( args );
                break;
            case 5:
                System.out.println( "Bye Bye!..." );
                break;
            default:
                System.out.println( "Choix non valide, réessayez !" );
                break;
            }
            scanner.close();
        } while ( choice == 0 || choice > 5 );

    }

    public static void timeTables( Scanner sc ) {
        Map<Integer, Study> studies = transformToMap( CourseManager.studies );
        int idStudy = 0;
        do {
            System.out.println( "Veuillez sélectionner la promotion concerné." );
            printMap( studies );
            idStudy = getInt( sc );
        } while ( !studies.containsKey( idStudy ) );

        System.out.println( "Veuillez saisir la date du cours (jj-mm-aaaa)" );
        LocalDate date = saisieDate( sc );
        LocalDateTime debut = LocalDateTime.of( date, LocalTime.of( 0, 0, 0 ) );
        Map<Integer, Book> books = transformToMap( BookManager.getBook( studies.get( idStudy ), debut ) );
        printMap( books );

    }
}
