package ja.view;

import static ja.view.TypeUtilities.getInt;
import static ja.view.TypeUtilities.printMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Stream;

import ja.controller.BuildingManager;
import ja.controller.ClassroomManager;
import ja.model.entities.Building;
import ja.model.entities.Classroom;

public class ViewClassroom {
    public static final String DRIVER   = "com.mysql.jdbc.Driver";

    public static final String URL      = "jdbc:mysql://localhost:3306/ja_db";
    public static final String USER     = "java";
    public static final String PASSWORD = "";

    public static void run( String[] args ) {

        int choice;

        System.out.println( "				 .. .. .. .. .. .. .. .. .. .. .. .. .. .. ... " );
        System.out.println( "				:					      :" );
        System.out.println( "				: 		Gestion des salles	      :" );
        System.out.println( "				:					      :" );
        System.out.println( "				 .. .. .. .. .. .. .. .. .. .. .. .. .. .. ... " );
        System.out.println();
        System.out.println();

        Stream<String> stream = Stream.of( "MENU" );
        stream.forEach(
                s -> System.out.println( "................................" + s + ".............................." ) );
        System.out.println();
        System.out.println( "	*	1.  Ajouter une nouvelle salle\t\t*" );
        System.out.println( "	*	2.  Supprimer une salle\t\t\t*" );
        System.out.println( "	*	3.  Modifier une salle\t\t\t*" );
        System.out.println( "	*	4.  Retour à l'accueil\t\t\t*" );
        System.out.println( "	*	5.  Quitter\t\t\t\t*" );
        System.out.println( ".................................................................." );

        do {
            System.out.println( "Entrez votre choix [1..5] :" );
            Scanner scanner = new Scanner( System.in );
            try {
                choice = scanner.nextInt();
            } catch ( Exception e ) {
                choice = 0;
            }
            switch ( choice ) {
            case 1:
                typeClassroom( scanner );
                ViewClassroom.run( args );
                break;
            case 2:
                deletingClassroom( scanner );
                ViewClassroom.run( args );
                break;
            case 3:
                updatingClassroom( scanner );
                ViewClassroom.run( args );
                break;
            case 4:
                System.out.flush();
                System.out.println( "Retour à l'accueil..." );
                MainView.main( args );
                System.out.println();
                break;
            case 5:
                System.out.println( "Bye Bye!..." );
                break;
            default:
                System.out.println( "Choix non valide, réessayez !" );
                break;
            }
            scanner.close();
        } while ( choice == 0 || choice > 6 );

    }

    public static void addClassroomAction( Scanner scanner ) {
        BuildingManager bManager = new BuildingManager();
        Map<Integer, Building> buildMap = bManager.getAsMap();
        int numBuild = 0;
        int idClass;
        int capacity;
        String type;
        do {

            System.out.println( "Veuillez selectionner le batiment ou se trouve la salle." );
            for ( int i : buildMap.keySet() ) {
                System.out
                        .println( i + " " + buildMap.get( i ).getIdBuilding() + " "
                                + buildMap.get( i ).getDescription() );
            }
            try {
                numBuild = Integer.parseInt( scanner.next() );
            } catch ( Exception e ) {
                numBuild = 0;
            }
            if ( !buildMap.containsKey( numBuild ) ) {
                System.err.println( "Batiment inconnu, veuiller faire un bon choix.\n" );

            }
        } while ( !buildMap.containsKey( numBuild ) );

        System.out.println( "Veuillez saisir le numero de la salle:" );
        idClass = scanner.nextInt();
        System.out.println( "Veuillez saisir la capacité de la salle:" );
        capacity = scanner.nextInt();
        System.out.println( "Veuillez saisir le type de la salle:" );
        type = scanner.nextLine();
        ClassroomManager cManager = new ClassroomManager();
        if ( cManager.addClassroom( idClass, buildMap.get( numBuild ).getIdBuilding(), capacity, type ) ) {
            System.out.println( "Salle ajoutée!!!" );
        } else {
            System.out.println( "Cette salle existe déjà!" );
        }
    }

    public static String selectBuilding( Scanner sc ) {
        BuildingManager bManager = new BuildingManager();
        Map<Integer, Building> buildMap = bManager.getAsMap();
        int numBuild = 0;

        do {

            System.out.println( "Veuillez selectionner le batiment ou se trouve la salle." );
            printMap( buildMap );
            numBuild = getInt( sc );
            if ( !buildMap.containsKey( numBuild ) ) {
                System.out.println( "Batiment inconnu, veuiller faire un bon choix.\n" );
            }
        } while ( !buildMap.containsKey( numBuild ) );

        return buildMap.get( numBuild ).getIdBuilding();
    }

    public static void typeClassroom( Scanner sc ) {
        String idBuilding = selectBuilding( sc );
        System.out.println( idBuilding );
        Map<Integer, String> type = new HashMap<Integer, String>();
        type.put( 1, Classroom.TYPE_LECTUREROOM );
        type.put( 2, Classroom.TYPE_COMPUTERROOM );
        type.put( 3, Classroom.TYPE_CHEMISTRYLAB );
        System.out.println( "Veuillez saisir le numero de la salle" );
        int idClassroom = getInt( sc );
        System.out.println( "Veuillez saisir la capacité de la salle" );
        int capacity = getInt( sc );
        System.out.println( "Veuillez sélectionner le type de la salle" );

        printMap( type );
        int typeId = getInt( sc );
        while ( typeId <= 0 || typeId > type.keySet().size() ) {
            printMap( type );
            System.err.println( "Veuillez saisir une capacité valide." );
            typeId = getInt( sc );
        }
        ClassroomManager cManager = new ClassroomManager();
        if ( cManager.addClassroom( idClassroom, idBuilding, capacity, type.get( typeId ) ) )

        {
            System.out.println( "Salle ajoutée avec succès!" );
        } else

        {
            System.out.println( "Echec de l'ajout, la salle existe déjà!" );
        }

    }

    public static void deletingClassroom( Scanner sc ) {
        String idBuilding = selectBuilding( sc );
        System.out.println( "Veuillez saisir le numero de la salle" );
        int idClassroom = getInt( sc );
        ClassroomManager cManager = new ClassroomManager();
        if ( cManager.deleteClassroom( idClassroom, idBuilding ) ) {
            System.out.println( "Salle supprimer avec succès!" );
        } else {
            System.out.println( "Echec de la suppression, cette salle n'existe pas." );
        }
    }

    public static void updatingClassroom( Scanner sc ) {
        System.out.println( "Fonctionnalité en conception!" );
    }
}
