package ja.view;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Classe ayant des méthodes utilitaires pour aider à la gestion de la vue
 * 
 * @author Groupe32
 *
 */
public class TypeUtilities {

    /**
     * Methode pour lire correctement un entier par l'entrée standard
     * 
     * @param sc
     *            :
     * @return entier lu
     */

    public static int getInt( Scanner sc ) {
        boolean badType = true;
        int i = 0;
        while ( badType ) {
            try {
                i = sc.nextInt();
                badType = false;
            } catch ( Exception e ) {
                sc.nextLine();
                System.out.println( "Saisie invalide, veuillez recommencer." );
            }
        }
        return i;
    }

    /**
     * Methode pour lire correctement une chaine de caracteres
     * 
     * @param sc
     * @return :chaine lue
     */
    public static String getString( Scanner sc ) {
        boolean badType = true;
        String s = null;
        while ( badType ) {
            s = sc.nextLine();
            if ( s.trim().isEmpty() || s.trim().equals( "" ) ) {
                System.out.println( "Saisie invalide, veuillez recommencer." );
            } else {
                badType = false;

            }
        }
        return s;
    }

    /**
     * Methode pour afficher une ma
     * 
     * @param map
     *            à afficher
     * 
     */
    public static <T, S> void printMap( Map<T, S> map ) {
        for ( T t : map.keySet() ) {
            if ( t != null )
                System.out.println( "\t" + t + " " + map.get( t ).toString() );
        }
    }

    /**
     * Methode pour extraire une heure au format LocalTime d'une date de type
     * LocalDateTime
     * 
     * @param date
     * @return : l'heure sous le format hh:mm
     */
    public static String getLocalTime( LocalDateTime date ) {
        String str = date.toString();
        return str.substring( str.lastIndexOf( "T" ) + 1, str.lastIndexOf( "T" ) + 6 );

    }

    /**
     * Methode pour extraire une date au format LocalDate dans une date de type
     * LocalDateTime
     * 
     * @param date
     * @return la date sous le format aaaa-mm-dd
     */
    public static String getLocalDate( LocalDateTime date ) {
        String str = date.toString();
        return str.toString().substring( 0, str.lastIndexOf( "T" ) );
    }

    /**
     * Methode pour lire une date au format LocalDate sur l'entrée standard
     * 
     * @param sc:
     * @return: la date lu
     */
    public static LocalDate saisieDate( Scanner sc ) {

        sc.nextLine();
        boolean badType = true;
        while ( badType ) {
            try {
                return LocalDate.parse( sc.nextLine(), DateTimeFormatter.ofPattern( "dd-MM-yyyy" ) );
            } catch ( Exception e ) {
                System.out.println( "Veuillez saisir une date respectant le format donné." );
                badType = true;
            }
        }
        return null;
    }

    /**
     * Methode pour lire une heure sur l'entrée standard
     * 
     * @param sc
     * @return :l'heure lue au format LocalTime
     */
    public static LocalTime saisieHeure( Scanner sc ) {

        boolean badType = true;
        while ( badType ) {
            try {
                String[] string = sc.nextLine().split( "-" );
                int hh = Integer.parseInt( string[0] );
                int mm = Integer.parseInt( string[1] );
                return LocalTime.of( hh, mm, 0 );
            } catch ( Exception e ) {
                System.out.println( "Veuillez saisir une heure respectant le format donné." );
                badType = true;
            }
        }
        return null;
    }

    /**
     * Methode pour convertir une collection est une map ayant des entiers comme
     * clés
     * 
     * @param c
     * @return map résultante
     */
    public static <T> Map<Integer, T> transformToMap( Collection<T> c ) {
        Map<Integer, T> map = new HashMap<Integer, T>();
        int i = 0;
        for ( T t : c ) {
            i++;
            map.put( i, t );
        }
        return map;
    }
}
