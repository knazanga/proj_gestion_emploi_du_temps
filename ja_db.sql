-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ja_db
-- ------------------------------------------------------
-- Server version	5.5.46-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `Book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Book` (
  `idClassroom` int(11) NOT NULL DEFAULT '0',
  `idBuilding` char(1) NOT NULL DEFAULT '',
  `idCourse` int(11) NOT NULL DEFAULT '0',
  `dateBook` date DEFAULT NULL,
  `startDate` datetime NOT NULL,
  `endDate` datetime NOT NULL,
  PRIMARY KEY (`idClassroom`,`idBuilding`,`idCourse`),
  KEY `fk_book_course` (`idCourse`),
  CONSTRAINT `fk_book_classroom` FOREIGN KEY (`idClassroom`, `idBuilding`) REFERENCES `Classroom` (`idClassroom`, `idBuilding`),
  CONSTRAINT `fk_book_course` FOREIGN KEY (`idCourse`) REFERENCES `Course` (`idCourse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Book`
--

LOCK TABLES `Book` WRITE;
/*!40000 ALTER TABLE `Book` DISABLE KEYS */;
INSERT INTO `Book` VALUES (1008,'P',18,'2016-01-10','2016-01-12 08:30:00','2016-01-12 10:30:00'),(2017,'P',19,'2016-01-10','2016-01-12 09:30:00','2016-01-12 11:30:00');
/*!40000 ALTER TABLE `Book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Building`
--

DROP TABLE IF EXISTS `Building`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Building` (
  `idBuilding` char(1) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idBuilding`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Building`
--

LOCK TABLES `Building` WRITE;
/*!40000 ALTER TABLE `Building` DISABLE KEYS */;
INSERT INTO `Building` VALUES ('E','Sciences Ecos'),('I','Administration'),('P','Sciences et Technologies');
/*!40000 ALTER TABLE `Building` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Classroom`
--

DROP TABLE IF EXISTS `Classroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Classroom` (
  `idClassroom` int(11) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `idBuilding` char(1) NOT NULL,
  PRIMARY KEY (`idClassroom`,`idBuilding`),
  KEY `fk_classroom_building` (`idBuilding`),
  CONSTRAINT `fk_classroom_building` FOREIGN KEY (`idBuilding`) REFERENCES `Building` (`idBuilding`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Classroom`
--

LOCK TABLES `Classroom` WRITE;
/*!40000 ALTER TABLE `Classroom` DISABLE KEYS */;
INSERT INTO `Classroom` VALUES (1008,'Salle pour cours magistral',50,'P'),(2017,'Salle pour cours magistral',45,'P');
/*!40000 ALTER TABLE `Classroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Course`
--

DROP TABLE IF EXISTS `Course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Course` (
  `idCourse` int(11) NOT NULL AUTO_INCREMENT,
  `type` char(2) DEFAULT NULL,
  `idTeacher` int(11) DEFAULT NULL,
  `idModule` int(11) DEFAULT NULL,
  `idGroup` int(11) DEFAULT NULL,
  `idStudy` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCourse`),
  KEY `fk_Course_Teacher1_idx` (`idTeacher`),
  KEY `fk_Course_Module1_idx` (`idModule`),
  KEY `fk_Course_study_idx` (`idStudy`),
  CONSTRAINT `fk_Course_Module1` FOREIGN KEY (`idModule`) REFERENCES `Module` (`idModule`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Course_study` FOREIGN KEY (`idStudy`) REFERENCES `Study` (`idStudy`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_Course_Teacher1` FOREIGN KEY (`idTeacher`) REFERENCES `Teacher` (`idTeacher`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Course`
--

LOCK TABLES `Course` WRITE;
/*!40000 ALTER TABLE `Course` DISABLE KEYS */;
INSERT INTO `Course` VALUES (3,'CM',1,3,NULL,NULL),(4,'CM',1,1,NULL,NULL),(5,'CM',1,4,NULL,NULL),(6,'TP',7,2,NULL,NULL),(7,'CM',11,5,NULL,NULL),(8,'CM',8,12,NULL,NULL),(9,'TP',3,7,NULL,NULL),(10,'TP',2,6,NULL,NULL),(13,'TP',2,3,1,8),(14,'TP',2,6,1,8),(15,'CM',12,12,NULL,NULL),(16,'TP',3,7,1,8),(17,'TP',4,9,1,8),(18,'CM',3,7,NULL,NULL),(19,'CM',8,12,NULL,NULL);
/*!40000 ALTER TABLE `Course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CourseTPSession`
--

DROP TABLE IF EXISTS `CourseTPSession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseTPSession` (
  `idCourse` int(11) NOT NULL,
  `idTPSession` int(11) NOT NULL,
  PRIMARY KEY (`idCourse`,`idTPSession`),
  KEY `fk_Course_has_TPSession_TPSession1_idx` (`idTPSession`),
  KEY `fk_Course_has_TPSession_Course1_idx` (`idCourse`),
  CONSTRAINT `fk_Course_has_TPSession_Course1` FOREIGN KEY (`idCourse`) REFERENCES `Course` (`idCourse`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Course_has_TPSession_TPSession1` FOREIGN KEY (`idTPSession`) REFERENCES `TPSession` (`idTPSession`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CourseTPSession`
--

LOCK TABLES `CourseTPSession` WRITE;
/*!40000 ALTER TABLE `CourseTPSession` DISABLE KEYS */;
/*!40000 ALTER TABLE `CourseTPSession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GroupAssignement`
--

DROP TABLE IF EXISTS `GroupAssignement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GroupAssignement` (
  `idGroup` int(11) NOT NULL,
  `idStudy` int(11) DEFAULT NULL,
  PRIMARY KEY (`idGroup`),
  KEY `fk_group_study` (`idStudy`),
  CONSTRAINT `fk_group_study` FOREIGN KEY (`idStudy`) REFERENCES `Study` (`idStudy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GroupAssignement`
--

LOCK TABLES `GroupAssignement` WRITE;
/*!40000 ALTER TABLE `GroupAssignement` DISABLE KEYS */;
INSERT INTO `GroupAssignement` VALUES (1,8),(2,8),(3,8);
/*!40000 ALTER TABLE `GroupAssignement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Module`
--

DROP TABLE IF EXISTS `Module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Module` (
  `idModule` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idModule`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Module`
--

LOCK TABLES `Module` WRITE;
/*!40000 ALTER TABLE `Module` DISABLE KEYS */;
INSERT INTO `Module` VALUES (1,'Complexité'),(2,'Base de données avancées'),(3,'Fonctionnement des réseaux'),(4,'Traitement d\'images'),(5,'Méthode de modélisation et optimisation'),(6,'Design Pattern'),(7,'Java avancée'),(8,'Initiation à la POO'),(9,'Programmation en Python'),(10,'Architecture des ordinateurs'),(11,'Logique et programmation'),(12,'Anglais');
/*!40000 ALTER TABLE `Module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ModuleStudy`
--

DROP TABLE IF EXISTS `ModuleStudy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModuleStudy` (
  `Module_idModule` int(11) NOT NULL,
  `idStudy` int(11) NOT NULL,
  PRIMARY KEY (`Module_idModule`,`idStudy`),
  KEY `fk_Module_has_Study_Study1_idx` (`idStudy`),
  KEY `fk_Module_has_Study_Module1_idx` (`Module_idModule`),
  CONSTRAINT `fk_Module_has_Study_Module1` FOREIGN KEY (`Module_idModule`) REFERENCES `Module` (`idModule`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Module_has_Study_Study1` FOREIGN KEY (`idStudy`) REFERENCES `Study` (`idStudy`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ModuleStudy`
--

LOCK TABLES `ModuleStudy` WRITE;
/*!40000 ALTER TABLE `ModuleStudy` DISABLE KEYS */;
INSERT INTO `ModuleStudy` VALUES (11,5),(12,5),(11,6),(12,6),(9,7),(12,7),(1,8),(2,8),(3,8),(4,8),(5,8),(6,8),(7,8),(9,8),(12,8),(7,9),(12,9),(12,10),(7,11),(11,11),(12,11),(11,12),(12,12);
/*!40000 ALTER TABLE `ModuleStudy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Student`
--

DROP TABLE IF EXISTS `Student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Student` (
  `idStudent` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `idStudy` int(11) DEFAULT NULL,
  `idGroup` int(11) DEFAULT NULL,
  PRIMARY KEY (`idStudent`),
  KEY `fk_Student_Study1_idx` (`idStudy`),
  KEY `fk_student_group` (`idGroup`),
  CONSTRAINT `fk_student_group` FOREIGN KEY (`idGroup`) REFERENCES `GroupAssignement` (`idGroup`),
  CONSTRAINT `fk_Student_Study1` FOREIGN KEY (`idStudy`) REFERENCES `Study` (`idStudy`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Student`
--

LOCK TABLES `Student` WRITE;
/*!40000 ALTER TABLE `Student` DISABLE KEYS */;
INSERT INTO `Student` VALUES (1,'Franck','NAZANGA',8,2),(2,'Ronny','GAGOUDE',8,1),(3,'Amirou','DIALLO',8,2),(5,'NOAH','Yannick',8,2),(6,'COULIBALY','Boubacar',8,2),(7,'DOUMBIA','Mamery',8,1),(8,'KANTE','Birime',8,1),(9,'MEDDAH','Amine',8,3);
/*!40000 ALTER TABLE `Student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StudentGroup`
--

DROP TABLE IF EXISTS `StudentGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StudentGroup` (
  `idStudent` int(11) NOT NULL,
  `idGroup` int(11) NOT NULL,
  `idModule` int(11) DEFAULT NULL,
  PRIMARY KEY (`idStudent`,`idGroup`),
  UNIQUE KEY `idStudent` (`idStudent`,`idGroup`,`idModule`),
  KEY `fk_Student_has_Group_Group1_idx` (`idGroup`),
  KEY `fk_Student_has_Group_Student1_idx` (`idStudent`),
  KEY `fk_student_grp_mod` (`idModule`),
  CONSTRAINT `fk_Student_has_Group_Group1` FOREIGN KEY (`idGroup`) REFERENCES `GroupAssignement` (`idGroup`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Student_has_Group_Student1` FOREIGN KEY (`idStudent`) REFERENCES `Student` (`idStudent`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StudentGroup`
--

LOCK TABLES `StudentGroup` WRITE;
/*!40000 ALTER TABLE `StudentGroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `StudentGroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Study`
--

DROP TABLE IF EXISTS `Study`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Study` (
  `idStudy` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idStudy`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Study`
--

LOCK TABLES `Study` WRITE;
/*!40000 ALTER TABLE `Study` DISABLE KEYS */;
INSERT INTO `Study` VALUES (5,'L1 INFO'),(6,'L2 INFO'),(7,'L3 INFO'),(8,'M1 INFO'),(9,'M2 SSI'),(10,'M2 IL'),(11,'L1 Maths'),(12,'L1 Biochimie');
/*!40000 ALTER TABLE `Study` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TPSession`
--

DROP TABLE IF EXISTS `TPSession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TPSession` (
  `idTPSession` int(11) NOT NULL,
  `idGroup` int(11) NOT NULL,
  PRIMARY KEY (`idTPSession`,`idGroup`),
  KEY `fk_TPSession_Group1_idx` (`idGroup`),
  CONSTRAINT `fk_TPSession_Group1` FOREIGN KEY (`idGroup`) REFERENCES `GroupAssignement` (`idGroup`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TPSession`
--

LOCK TABLES `TPSession` WRITE;
/*!40000 ALTER TABLE `TPSession` DISABLE KEYS */;
/*!40000 ALTER TABLE `TPSession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Teacher`
--

DROP TABLE IF EXISTS `Teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Teacher` (
  `idTeacher` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idTeacher`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Teacher`
--

LOCK TABLES `Teacher` WRITE;
/*!40000 ALTER TABLE `Teacher` DISABLE KEYS */;
INSERT INTO `Teacher` VALUES (1,'Alexis','BES'),(2,'Sovanna','TAN'),(3,'Daniele','VARACCA'),(4,'Pascal','VANIER'),(5,'Lynda','MOKDAD'),(6,'Youssouf','OUALHADJ'),(7,'Luidnel','MAIGNAN'),(8,'Cecile','LORIN'),(9,'Nihal','PEKERGIN'),(10,'Julien','CERVELLE'),(11,'Catalin','DIMA'),(12,'Nicolas','DEBASTE');
/*!40000 ALTER TABLE `Teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TeacherModule`
--

DROP TABLE IF EXISTS `TeacherModule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TeacherModule` (
  `idTeacher` int(11) NOT NULL DEFAULT '0',
  `idModule` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idTeacher`,`idModule`),
  KEY `fk_teach_mod2` (`idModule`),
  CONSTRAINT `fk_teach_mod` FOREIGN KEY (`idTeacher`) REFERENCES `Teacher` (`idTeacher`),
  CONSTRAINT `fk_teach_mod2` FOREIGN KEY (`idModule`) REFERENCES `Module` (`idModule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TeacherModule`
--

LOCK TABLES `TeacherModule` WRITE;
/*!40000 ALTER TABLE `TeacherModule` DISABLE KEYS */;
INSERT INTO `TeacherModule` VALUES (1,1),(7,2),(2,3),(5,3),(1,4),(6,5),(11,5),(2,6),(3,6),(10,6),(3,7),(10,7),(3,8),(4,8),(10,8),(3,9),(4,9),(5,10),(7,11),(8,12),(12,12);
/*!40000 ALTER TABLE `TeacherModule` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-10 18:15:25
